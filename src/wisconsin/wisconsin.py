# -*- coding: utf-8 -*-
"""
###############################################
AgileUFRJ - Implementando as teses do PPGI
###############################################
:Author: *Carlo E. T. Oliveira*
:Contact: carlo@nce.ufrj.br
:Date: $Date: 2010/09/07 $
:Status: This is a "work in progress"
:Revision: $Revision: 0.02 $
:Home: `LABASE <http://labase.nce.ufrj.br/>`__
:Copyright: ©2011, `GPL <http://is.gd/3Udt>`__.
"""

import wcst
# import wisconsin.wcst as wcst
from browser import html, document

# from plugins.api import model as model


WISCPAGE = "wisconsin.html"

CRITERIA = ['carta_resposta', 'categoria', 'acertos', 'cor', 'forma', 'numero', 'outros', 'time']
HEADINGS = ['Carta resposta', 'Categoria', 'Acertos', 'Cor', 'Forma', u'Número', 'Outros', 'Data/Hora']

'''
class WisconsinHandler:
    """
    Joga o Teste de Wisconsin
    """

    def index(self, init="2", level="1"):
        self.title = "Wisconsin"
        houses = {"indiceCartaAtual": -1,
                  "categoria": 0,
                  "acertosConsecutivos": 0,
                  "outrosConsecutivos": 0,
                  "wteste": None
                  }

        sessionid = self.get_current_gamesession()
        self._wisc = model.API_GAME().retrieve(sessionid)
        if init == "0":  # nova tentativa
            self._wisc.next(newtrial=True, houses=houses, markers=[], table=[])
        elif init == "1":  # novo nível
            self._wisc.next(newlevel=int(level), houses=houses, markers=[], criteria=CRITERIA, headings=HEADINGS)
        elif init == "2":  # novo jogo
            self._wisc.next(newgame="wisconsin", maxlevel=1, newlevel=int(level), houses=houses, markers=[],
                            criteria=CRITERIA, headings=HEADINGS)

        self.redirect("/wisconsin/play")

    def play(self, result="", **kargs):
        sessionid = 0
        cartasEstimulo = []
        cartaPuxada = None
        if result != "Fim do Jogo":
            self._wisc = model.API_GAME().retrieve(sessionid)
            self._wisc.houses["indiceCartaAtual"] = self._wisc.houses["indiceCartaAtual"] + 1
            indiceCartaAtual = self._wisc.houses["indiceCartaAtual"]
            self._wisc.next(houses=self._wisc.houses)

            cartasEstimulo = wcst.listaCartasEstimulo
            cartaPuxada = wcst.listaCartasResposta[indiceCartaAtual]

        self.render(WISCPAGE, CRITERIA=CRITERIA, \
                    CARTAPUXADA=cartaPuxada, CARTASESTIMULO=cartasEstimulo, \
                    MSG=result, \
                    SESSIONID=sessionid, RESULT=model.RESULT)

    def click(self, opcao, **kargs):
        opcao = int(opcao)
        sessionid = self.get_current_gamesession()

        self._wisc = model.API_GAME().retrieve(sessionid)
        indiceCartaAtual = self._wisc.houses["indiceCartaAtual"]
        categoria = self._wisc.houses["categoria"]
        acertosConsecutivos = self._wisc.houses["acertosConsecutivos"]
        outrosConsecutivos = self._wisc.houses["outrosConsecutivos"]

        indiceCarta = (indiceCartaAtual % wcst.numCartasResposta) + 1
        cartaResposta = wcst.listaCartasResposta[indiceCartaAtual]
        cartaEstimulo = wcst.listaCartasEstimulo[opcao]

        tudoDiferente = cartaResposta.testaTudoDiferente(cartaEstimulo)

        if cartaResposta.testaMesmaCategoria(cartaEstimulo,
                                             wcst.listaCategorias[categoria]):
            acertosConsecutivos += 1
            resultadoTeste = "Certo"
        else:
            acertosConsecutivos = 0
            resultadoTeste = "Errado"

        if tudoDiferente:
            outrosConsecutivos += 1
        else:
            outrosConsecutivos = 0

        if outrosConsecutivos == 3:
            outrosConsecutivos = 0
            resultadoTeste = u"%s.<br/>Leia atentamente as instruções: %s" % (resultadoTeste, wcst.instrucoes_teste())

        # Grava a jogada no banco de dados
        table = [dict(
            carta_resposta=indiceCarta,
            categoria=wcst.listaCategorias[categoria],
            acertos=acertosConsecutivos,
            cor=cartaResposta.testaMesmaCategoria(cartaEstimulo,
                                                  wcst.listaCategorias[0]),
            forma=cartaResposta.testaMesmaCategoria(cartaEstimulo,
                                                    wcst.listaCategorias[1]),
            numero=cartaResposta.testaMesmaCategoria(cartaEstimulo,
                                                     wcst.listaCategorias[2]),
            outros=tudoDiferente,
            time=str(datetime.datetime()))]

        # Se os acertos consecutivos chegarem a 10, troca a categoria
        if acertosConsecutivos == 10:
            acertosConsecutivos = 0
            categoria += 1

        houses = {"indiceCartaAtual": indiceCartaAtual,
                  "categoria": categoria,
                  "acertosConsecutivos": acertosConsecutivos,
                  "outrosConsecutivos": outrosConsecutivos,
                  "wteste": None
                  }
        self._wisc.next(houses=houses, table=table)

        # Termina o teste se esgotar as categorias ou fim das cartas respostas.
        if ((categoria >= len(wcst.listaCategorias)) or
                (indiceCartaAtual >= len(wcst.listaCartasResposta) - 1)):
            resultadoTeste = "Fim do Jogo"

        params = {'username': 'administrator', 'password': 'xyz'}
        self.redirect('/wisconsin/play?' + urllib.parse.urlencode(params))

    def render(self, WISCPAGE, CRITERIA, CARTAPUXADA, CARTASESTIMULO, MSG, SESSIONID, RESULT):
        pass

    def redirect(self, param):
        pass
'''


class Jogo:
    def __init__(self):
        self.tabuleiro = document["main_content"]
        self.msg = ""
        self.cartapuxada = wcst.listaCartasResposta.pop(0)
        self.cartasestimulo = wcst.listaCartasEstimulo

        if self.msg:
            html.DIV(self.msg, id="status_block", Class="flash")

        if self.cartapuxada:
            self.divout = html.DIV()
            self.tabuleiro <= self.divout
            self.puxa_carta(self.divout)

            self.tabuleiro <= html.P("Combina com qual das cartas abaixo?")
            ul = html.UL()
            self.tabuleiro <= ul
            for (indice, carta) in enumerate(self.cartasestimulo):
                href = f"click?opcao={indice}"
                img = f"img/{carta.img}"
                alt = f"{carta.pegaAtributosCarta()}"
                title = f"{carta.pegaAtributosCarta()}"
                li = html.LI()
                ul <= li
                hpin = html.IMG(src=img, alt=alt, title=title)
                # hpin.bind("click", self.onclick)

                div = html.DIV(Id=f"a_carta_{indice}", Class=f"carta {carta.color}")
                div <= hpin
                li <= div
                div.bind("click", self.onclick)

                for item in range(carta.num - 1):
                    img = f"img/{carta.img}"
                    hpin = html.IMG(src=img)
                    hpin.bind("click", self.onclick)
                    div.bind("click", self.onclick)
                    div <= hpin

    def puxa_carta(self, div_carta):
        div_carta.html = ""
        divout = html.DIV(Class=f"carta puxada {self.cartapuxada.color}")
        div_carta <= divout
        img = f"img/{self.cartapuxada.img}"
        alt = f"{self.cartapuxada.pegaAtributosCarta()}"
        title = f"{self.cartapuxada.pegaAtributosCarta()}"
        hpin = html.IMG(src=img, alt=alt, title=title)
        divout <= hpin
        for item in range(self.cartapuxada.num - 1):
            img = f"img/{self.cartapuxada.img}"
            hpin = html.IMG(src=img)
            divout <= hpin

    def onclick(self, ev):
        print(ev.target.id)
        self.cartapuxada = wcst.listaCartasResposta.pop(0)
        self.puxa_carta(self.divout)


def main():
    Jogo()
